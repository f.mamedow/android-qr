package fm.android.qr.fragments.scan

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.ScanMode
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import fm.android.qr.R
import fm.android.qr.databinding.FragmentScannerBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FragmentQrScanner : Fragment() {

    private var binding: FragmentScannerBinding? = null

    private var codeScanner : CodeScanner? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_scanner, container, false)
        binding = FragmentScannerBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupQrScanner()
    }

    private fun setupQrScanner() {
        binding?.apply {
            codeScanner = CodeScanner(requireContext(), codeScannerView)

            codeScanner?.camera = CodeScanner.CAMERA_BACK
            codeScanner?.formats = CodeScanner.ALL_FORMATS
            codeScanner?.autoFocusMode = AutoFocusMode.SAFE
            codeScanner?.scanMode = ScanMode.SINGLE
            codeScanner?.isAutoFocusEnabled = true
            codeScanner?.isFlashEnabled = false

            codeScanner?.setDecodeCallback {
                onDecode(it.text)
            }


            codeScanner?.setErrorCallback {
                it.printStackTrace()
                onDecodeError(it.message)
            }


            codeScannerView.setOnClickListener {
                if (checkPermission()) {
                    codeScanner?.startPreview()
                } else {
                    requestPermission()
                }
            }
        }
    }

    private fun onDecode(content: String) = lifecycleScope.launch(Dispatchers.Main) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.qr_code_title))
            .setMessage(content)
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
                codeScanner?.startPreview()
            }
            .show()
    }

    private fun onDecodeError(message: String?) = lifecycleScope.launch(Dispatchers.Main) {
        Toast.makeText(requireContext(), "Result $message", Toast.LENGTH_SHORT).show()
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(Manifest.permission.CAMERA),
            CAMERA_PERMISSION_REQUEST_CODE
        )
    }

    override fun onResume() {
        super.onResume()
        codeScanner?.startPreview()
    }

    override fun onPause() {
        codeScanner?.releaseResources()
        super.onPause()
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    companion object {
        const val CAMERA_PERMISSION_REQUEST_CODE = 10
    }

}