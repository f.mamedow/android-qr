package fm.android.qr.fragments.generate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import fm.android.qr.R
import fm.android.qr.databinding.FragmentGeneratorBinding
import fm.android.qr.utils.generateQR
import kotlinx.coroutines.launch

class FragmentQrGenerator : Fragment() {

    private var binding: FragmentGeneratorBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_generator, container, false)
        binding = FragmentGeneratorBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupInput()
    }

    private fun setupInput() {
        binding?.apply {
            etData.addTextChangedListener { text ->
                generateQrCode(text.toString())
            }
        }
    }

    private fun generateQrCode(content: String) {
        binding?.apply {

            val qrCodeBitmap = generateQR(content, QR_CODE_SIZE)
            ivQr.setImageBitmap(qrCodeBitmap)
        }
    }


    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }


    companion object {
        private const val QR_CODE_SIZE = 400
    }

}