package fm.android.qr.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import fm.android.qr.R
import fm.android.qr.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var binding : FragmentHomeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        binding = FragmentHomeBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding?.btnGenerate?.setOnClickListener {
            it.findNavController().navigate(R.id.qr_generator_fragment)
        }
        binding?.btnScanner?.setOnClickListener {
            it.findNavController().navigate(R.id.qr_scanner_fragment)
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

}